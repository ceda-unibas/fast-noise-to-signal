import torch

import numpy as np
import matplotlib.pyplot as plt


def convolve1d(signal, kernel, stride=None):
    """Convolve a 1D signal with a 1D kernel.

    Parameters
    ----------
    signal : torch.Tensor
        1D signal to convolve. Implied shape is (batch_size, channels, time).
    kernel : list
        1D kernel to convolve with.
    stride : int, optional
        Stride of the convolution. If not specified, it is set to the length of
        the kernel.

    Returns
    -------
    torch.Tensor
        Convolved signal.
    """
    # Signal is a torch tensor of shape (batch_size, channels, time)
    c = signal.shape[1]

    # Define filter from kernel
    filter = torch.FloatTensor(kernel).repeat(c, 1, 1)

    # Parse stride
    stride = len(kernel) if stride is None else stride

    # Apply filter
    return torch.nn.functional.conv1d(signal, filter, stride=stride, groups=c)


def random_split(window_length):
    if window_length <= 2:
        return split_and_squeeze_kernels(randomize=False,
                                         window_length=window_length)

    # Draw a random binary sequence of length window_length, making sure that
    # it is neither all zeros nor all ones
    binary_sequence = np.zeros((window_length,))
    while np.all(binary_sequence == 0) or np.all(binary_sequence == 1):
        binary_sequence = np.random.randint(0, 2, size=window_length)

    # Define a complementary binary sequence
    complementary_sequence = 1 - binary_sequence

    # Return the two sequences
    return list(binary_sequence), list(complementary_sequence)


def split_and_squeeze_kernels(randomize=False, window_length=2):
    if randomize:
        kernel1, kernel2 = random_split(window_length=window_length)
    else:
        kernel1 = [1, 0]
        kernel2 = [0, 1]
        for _ in range(window_length - 2):
            kernel1 += [1 - kernel1[-1]]
            kernel2 += [1 - kernel2[-1]]

    return kernel1, kernel2


def split_and_squeeze(signal):
    """Split-and-squeeze filter.

    Parameters
    ----------
    signal : torch.Tensor
        1D signal to split. Implied shape is `(batch_size, channels, time)`.
    **kwargs
        Extra keyword arguments to pass to convolve1d.

    Returns
    -------
    tuple
        Tuple of two torch.Tensors, each of shape
        `(batch_size, channels, time)`.
    """
    # Get the splitting kernels
    kernel1 = [1, 0]
    kernel2 = [0, 1]

    split1 = convolve1d(signal, kernel=kernel1)
    split2 = convolve1d(signal, kernel=kernel2)

    return split1, split2


def split_and_average_kernels(randomize=False, window_length=4):
    if randomize:
        kernel1, kernel2 = random_split(window_length=window_length)
    else:
        # Round window_length to the nearest even number
        window_length = 2 * (window_length // 2)

        kernel1 = [1] * (window_length // 2) + [0] * (window_length // 2)
        kernel2 = [0] * (window_length // 2) + [1] * (window_length // 2)

    # Normalize the kernels so that they sum to 1 and therefore act as
    # averaging filters
    kernel1 = [k / sum(kernel1) for k in kernel1]
    kernel2 = [k / sum(kernel2) for k in kernel2]

    return kernel1, kernel2


def split_and_average(signal, randomize=False, window_length=4):
    """Split-and-average filter.

    Parameters
    ----------
    signal : torch.Tensor
        1D signal to split. Implied shape is `(batch_size, channels, time)`.
    **kwargs
        Extra keyword arguments to pass to convolve1d.

    Returns
    -------
    tuple
        Tuple of two torch.Tensors, each of shape
        `(batch_size, channels, time)`.
    """
    # Get the splitting kernels
    # kernel1 = [0.5, 0.5, 0, 0]
    # kernel2 = [0, 0, 0.5, 0.5]
    kernel1, kernel2 = split_and_average_kernels(
        randomize=randomize,
        window_length=window_length
    )

    # Obtain splits by applying the split-and-average filter
    split1 = convolve1d(signal, kernel=kernel1)
    split2 = convolve1d(signal, kernel=kernel2)

    return split1, split2


def recursive_split(signal, split_fun, depth=1):
    """Recursively split a signal.

    Parameters
    ----------
    signal : torch.Tensor
        1D signal to split. Implied shape is `(batch_size, channels, time)`.
    split_fun : function
        Function to use to split the signal. Must take a signal as input and
        return a tuple of two signals as output.
    depth : int, optional
        Depth of the recursion. Default is 1.

    Returns
    -------
    tuple
        Tuple of torch.Tensors, each of shape (batch_size, channels, time).
    """
    if depth <= 0:
        return (signal, )
    else:
        split1, split2 = split_fun(signal)
        return recursive_split(split1, split_fun, depth - 1) + \
            recursive_split(split2, split_fun, depth - 1)


def psnr(x, y):
    mse = np.mean((x - y) ** 2)
    if mse == 0:
        return 100
    max_val = np.max(x)
    return 10 * np.log10((max_val**2) / mse)


def plot_denoising(noisy_signal, denoised_signal, title=""):

    fig, ax = plt.subplots(2, 1, figsize=(10, 5), sharex=True, sharey=True)
    ax[0].plot(noisy_signal)
    ax[0].set_ylabel("Noisy signal")
    ax[0].set_title(title)
    ax[1].plot(denoised_signal)
    ax[1].set_ylabel("Denoised signal")

    return fig


def total_gradient_norm(pl_module, norm_type=2):
    """Compute the total norm of the gradients of a model's parameters.

    Parameters
    ----------
    pl_model : pytorch_lightning.LightningModule
        Model to compute the gradient norm of.
    norm_type : int, float, numpy.inf, torch.inf, optional
        Type of norm to compute. Default is 2.

    Returns
    -------
    float
        Total norm of the gradients.
    """
    total_norm = 0.0
    for param in pl_module.parameters():
        if param.grad is not None:
            param_norm = param.grad.detach().data.norm(norm_type)
            if isinstance(norm_type, (int, float)) and norm_type > 0:
                total_norm += param_norm.item() ** norm_type
            elif isinstance(norm_type, (np.inf, torch.inf)):
                total_norm = max(total_norm, param_norm.item())
            else:
                total_norm += param_norm.item()

    if isinstance(norm_type, (int, float)) and norm_type > 0:
        total_norm = total_norm ** (1. / norm_type)

    return total_norm


class SingletonDataset(torch.utils.data.Dataset):
    """Dataset containing a single signal.

    Parameters
    ----------
    signal : numpy.ndarray
        1D signal to use as dataset. Implied shape is `(time,)`.
    yield_normalized : bool, optional
        Whether to yield the normalized signal via the `__getitem__` method.
        Default is False.
    """
    def __init__(self, signal, yield_normalized=False):
        self.signal = signal.reshape(1, len(signal))
        self.max_val = signal.max()
        self.min_val = signal.min()
        self.normalized_signal = (self.signal - self.min_val) / \
            (self.max_val - self.min_val)
        self.yield_normalized = yield_normalized

    def __len__(self):
        return 1

    def __getitem__(self, idx):
        if self.yield_normalized:
            signal = self.normalized_signal
        else:
            signal = self.signal

        # Turn signal into a torch tensor
        out = torch.tensor(signal, dtype=torch.float32)

        return out


