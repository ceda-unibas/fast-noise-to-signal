
import torch

import numpy as np

from fn2s import utils
from lightning.pytorch.callbacks import Callback
from lightning.pytorch.utilities import grad_norm


class DenoisingPlotCallback(Callback):
    def __init__(self, signal_tensor, every_n_epochs=10):
        super().__init__()
        self.signal_tensor = signal_tensor
        self.every_n_epochs = every_n_epochs

        # Keep a copy of the original signal as a flattened numpy array
        self.signal = signal_tensor.detach().cpu().numpy().flatten()

    def on_validation_epoch_end(self, trainer, pl_module):
        if trainer.current_epoch % self.every_n_epochs == 0:
            # Denoise the signal tensor via the model
            denoised_signal = pl_module.denoise(self.signal_tensor)

            # Convert the denoised signal to a flattened numpy array
            denoised_signal = denoised_signal.detach().cpu().numpy().flatten()

            # Log the denoised signal to TensorBoard
            trainer.logger.experiment.add_figure(
                "denoised_sample",
                utils.plot_denoising(
                    self.signal,
                    denoised_signal,
                    title="Epoch {}".format(trainer.current_epoch)),
                trainer.current_epoch
            )


class TotalGradNormCallback(Callback):
    """Callback to log the gradient norm of the model's parameters.

    Parameters
    ----------
    norm_type : int, float, numpy.inf, torch.inf, optional
        Type of norm to compute. Default is 2.
    every_n_steps : int, optional
        Log every `every_n_steps` steps. Default is 10.

    Notes
    -----
    By default, `lightning.Trainer(..., track_grad_norm=2)` logs individual
    norms of all gradients. This can cause clutter in the TensorBoard logs.
    This callback logs the total norm of all gradients instead. This solution
    is based on the discussion in this GitHub issue:
    https://github.com/Lightning-AI/pytorch-lightning/issues/1462

    See Also
    --------
    total_gradient_norm
    """
    def __init__(self, norm_type=2, every_n_steps=10):
        super().__init__()
        self.norm_type = norm_type
        self.every_n_steps = every_n_steps

    def on_before_optimizer_step(self, trainer, pl_module, optimizer):
        if trainer.global_step % self.every_n_steps == 0:
            if isinstance(self.norm_type, (int, float)):
                norm_type_str = "{:.1f}".format(self.norm_type)
            elif isinstance(self.norm_type, (np.inf, torch.inf)):
                norm_type_str = "inf"
            else:
                norm_type_str = "{}".format(self.norm_type)
            pl_module.log(
                "total_grad_norm_{}".format(norm_type_str),
                utils.total_gradient_norm(pl_module, self.norm_type),
            )


class GradHistCallback(Callback):
    """Callback to log the histogram of the gradients of the model's
    parameters.

    Parameters
    ----------
    norm_type : int, float, numpy.inf, torch.inf, optional
        Type of norm to compute. Default is 2.
    every_n_steps : int, optional
        Log every `every_n_steps` steps. Default is 10.
    """
    def __init__(self, norm_type=2, every_n_steps=10):
        super().__init__()
        self.every_n_steps = every_n_steps
        self.norm_type = norm_type

    def on_before_optimizer_step(self, trainer, pl_module, optimizer):
        if trainer.global_step % self.every_n_steps == 0:
            # Gather a tensor of gradient norms
            grad_norms = torch.tensor([
                param.grad.norm(self.norm_type)
                for param in pl_module.parameters()
                if param.grad is not None
            ])

            # Log the histogram of the gradient norms to TensorBoard
            trainer.logger.experiment.add_histogram(
                "grad_norms_hist",
                grad_norms,
                bins="auto"
            )


class LayerGradNormCallback(Callback):
    """Callback to log the norm of the gradients each layer of the model

    Parameters
    ----------
    norm_type : int, float, str
        Type of norm to compute. Default is 2. See
        `lightning.pytorch.utilities.grad_norm` for more details.
    every_n_steps : int, optional
        Log every `every_n_steps` steps. Default is 10.
    """
    def __init__(self, norm_type=2, every_n_steps=10):
        super().__init__()
        self.every_n_steps = every_n_steps
        self.norm_type = norm_type

    def on_before_optimizer_step(self, trainer, pl_module, optimizer):
        if trainer.global_step % self.every_n_steps == 0:
            norms = grad_norm(pl_module, norm_type=self.norm_type)
            self.log_dict(norms)
