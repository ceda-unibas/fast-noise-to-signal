import torch

from fn2s import losses, utils
from lightning import LightningModule
from torch import nn, optim
from torch.nn.functional import mse_loss


class TwoConv(nn.Module):
    """Module network with two 1D convolutional layers with ReLU activation

    Parameters
    ----------
    in_channels : int
        Number of input channels.
    out_channels : int
        Number of output channels.

    Attributes
    ----------
    conv1 : torch.nn.Conv1d
        First convolutional layer.
    conv2 : torch.nn.Conv1d
        Second convolutional layer.
    """

    def __init__(self, in_channels, out_channels):
        super().__init__()
        self.conv1 = nn.Conv1d(in_channels, out_channels, 3, padding=1)
        self.conv2 = nn.Conv1d(out_channels, out_channels, 3, padding=1)

    def forward(self, x):
        x = self.conv1(x)
        x = nn.functional.relu(x)
        x = self.conv2(x)
        x = nn.functional.relu(x)
        return x


class FCNLarge(nn.Module):
    """Large fully convolutional network formed by stacking TwoConv blocks
    blocks

    Parameters
    ----------
    sigmoid_output : bool, optional
        Whether to apply a sigmoid activation to the output. Default is False.

    Attributes
    ----------
    conv1 : TwoConv
        First convolutional block.
    conv2 : TwoConv
        Second convolutional block.
    conv3 : TwoConv
        Third convolutional block.
    conv4 : TwoConv
        Fourth convolutional block.
    conv5 : TwoConv
        Fifth convolutional block.
    conv6 : torch.nn.Conv1d
        Last convolutional layer.
    last_op : torch.nn.Module
        Last operation to apply to the output.

    Notes
    -----
    This architecture is based on the one used in [1]_.

    References
    ----------
    .. [1] Lequyer, J., Philip, R., Sharma, A., Hsu, W.-H., & Pelletier, L.
    (2022). A fast blind zero-shot denoiser. Nature Machine Intelligence,
    4(11, 11), 953-963. https://doi.org/10.1038/s42256-022-00547-8
    """
    def __init__(self, sigmoid_output=False):
        super().__init__()
        self.conv1 = TwoConv(1, 64)
        self.conv2 = TwoConv(64, 64)
        self.conv3 = TwoConv(64, 64)
        self.conv4 = TwoConv(64, 64)
        self.conv6 = nn.Conv1d(64, 1, 1)
        if sigmoid_output:
            self.last_op = nn.Sigmoid()
        else:
            self.last_op = nn.Identity()

    def forward(self, x):
        x1 = self.conv1(x)
        x2 = self.conv2(x1)
        x3 = self.conv3(x2)
        x = self.conv4(x3)
        x = self.conv6(x)
        x = self.last_op(x)
        return x


class FCNSmall(nn.Module):
    """Small fully convolutional network formed by stacking 1D convolutional
    layers with LeakyReLU activations

    Parameters
    ----------
    in_channels : int, optional
        Number of input channels, by default 1.
    embed_channels : int, optional
        Number of channels in the embedding layer, by default 48.
    sigmoid_output : bool, optional
        Whether to apply a sigmoid activation to the output. Default is False.

    Attributes
    ----------
    activation : torch.nn.LeakyReLU
        LeakyReLU activation function.
    conv1 : torch.nn.Conv1d
        First convolutional layer.
    conv2 : torch.nn.Conv1d
        Second convolutional layer.
    conv3 : torch.nn.Conv1d
        Third convolutional layer.

    Notes
    -----
    This architecture is based on the one used in [1]_.

    References
    ----------
    .. [1] Mansour, Y., & Heckel, R. (2023). Zero-Shot Noise2Noise: Efficient
    Image Denoising without any Data. 2023 IEEE/CVF Conference on Computer
    Vision and Pattern Recognition (CVPR), 14018-14027. Vancouver, BC, Canada:
    IEEE. https://doi.org/10.1109/CVPR52729.2023.01347
    """
    def __init__(self, in_channels=1, embed_channels=48, sigmoid_output=False):
        super(FCNSmall, self).__init__()

        self.activation = nn.LeakyReLU(negative_slope=0.2, inplace=True)
        self.conv1 = nn.Conv1d(in_channels, embed_channels, 3, padding=1)
        self.conv2 = nn.Conv1d(embed_channels, embed_channels, 3, padding=1)
        self.conv3 = nn.Conv1d(embed_channels, in_channels, 1)
        if sigmoid_output:
            self.last_op = nn.Sigmoid()
        else:
            self.last_op = nn.Identity()

    def forward(self, x):
        x = self.activation(self.conv1(x))
        x = self.activation(self.conv2(x))
        x = self.conv3(x)
        x = self.last_op(x)
        return x


class FN2S(LightningModule):
    """Fast Noise2Signal model

    Parameters
    ----------
    split_fun : function, optional
        Function to use to split the signal into a training batch. Must take a
        signal as input and return a tuple of signals as output. Default is
        None. If None, `utils.split_and_squeeze` is used.
    net : torch.nn.Module, optional
        Network architecture to use. Default is None. If None, `FCNSmall` is
        used. The provided network must have outputs in the range [0, 1].
    training_type : str, optional
        Type of training to use. Default is "stopped".

    Attributes
    ----------
    split_fun : function
        Function to use to split the signal into a training batch.
    net : torch.nn.Module
        Network architecture to use.
    training_type : str
        Type of training to use.
    """
    def __init__(self,
                 split_fun=None,
                 net=None,
                 training_type="residual_regularized"):
        super(FN2S, self).__init__()

        # Set training type
        self._TRAINING_OPTIONS = ["bce_stopped", "residual_regularized"]
        if training_type not in self._TRAINING_OPTIONS:
            raise ValueError(
                f"training must be one of {self._TRAINING_OPTIONS}"
            )
        self.training_type = training_type

        # Set splitting function
        self.split_fun = utils.split_and_squeeze if split_fun is None \
            else split_fun

        # Set network architecture
        self.net = FCNSmall() if net is None else net

        # Save hyperparameters
        self.save_hyperparameters(ignore=["net"])

    def forward(self, x):
        return self.net(x)

    def denoise(self, x):
        with torch.no_grad():
            if self.training_type == "bce_stopped":
                # Normalize the signal to [0, 1]
                max_val = x.max()
                min_val = x.min()
                x = x - min_val
                x = x / (max_val - min_val)

                # Network output is the normalized signal
                out = self(x)

                return out * (max_val - min_val) + min_val

            elif self.training_type == "residual_regularized":
                # Network output is the residual
                return x - self(x)

            else:
                return x

    def training_step(self, batch):
        if self.training_type == "bce_stopped":
            loss = losses.bce_split_pairs(self, batch)
        elif self.training_type == "residual_regularized":
            loss = losses.residual_plus_consistency(self, batch)
        else:
            raise ValueError(
                f"training must be one of {self._TRAINING_OPTIONS}"
            )

        # Log loss to TensorBoard
        self.log("train_loss", loss, prog_bar=True)

        return loss

    def validation_step(self, batch):
        # We just need to keep track of how close is the map from the input
        # to its denoised version. We can use the mean squared error for this.
        loss = mse_loss(self(batch), batch)

        # Log loss to TensorBoard
        self.log("val_loss", loss, prog_bar=True)

    def configure_optimizers(self):
        if self.training_type == "bce_stopped":
            optimizer = optim.Adam(self.parameters(), lr=0.001)
            scheduler = optim.lr_scheduler.StepLR(optimizer,
                                                  step_size=100,
                                                  gamma=0.5)
        elif self.training_type == "residual_regularized":
            optimizer = optim.Adam(self.parameters(), lr=0.001)
            scheduler = optim.lr_scheduler.StepLR(optimizer,
                                                  step_size=1000,
                                                  gamma=0.5)
        else:
            raise ValueError(
                f"training must be one of {self._TRAINING_OPTIONS}"
            )
        return {"optimizer": optimizer, "lr_scheduler": scheduler}
