"""Signal generating module."""
import numpy as np


def make_noisy_exp_decay_train(n_signals=1,
                               n_steps=1000,
                               n_events=10,
                               lam=100,
                               tau=30,
                               sigma=0.1):
    """Generate a dataset of noisy signals with an exponential decay kernel.

    Parameters
    ----------
    n_signals : int, optional
        Number of signals to generate. Default is 1.
    n_steps : int, optional
        Number of time steps for each signal. Default is 1000.
    n_events : int, optional
        Number of events to generate for each signal. Default is 10.
    lam : float or list, optional
        Mean rate of the Poisson distribution from which to draw the events.
        Default is 100. If a list, must have the same length as `n_signals`.
    tau : float or list, optional
        Time constant of the exponential decay kernel. Default is 30. If a
        list, must have the same length as `n_signals`.
    sigma : float or list, optional
        Standard deviation of the zero-mean Gaussian noise to add to the
        signals. Default is 0.1. If a list, must have the same length as
        `n_signals`.

    Returns
    -------
    tuple
        Tuple of two numpy arrays. The first array contains the noisy signals
        and has shape `(n_signals, n_steps)`. The second array contains the
        ground-truth signals and has the same shape.
    """
    noisy_signals = []
    gt_signals = []

    # Parse input arguments. If they are int or floats, turn them into
    # lists of the same length as n_signals
    if isinstance(n_steps, (int, float)):
        n_steps = [n_steps] * n_signals
    if isinstance(lam, (int, float)):
        lam = [lam] * n_signals
    if isinstance(tau, (int, float)):
        tau = [tau] * n_signals
    if isinstance(sigma, (int, float)):
        sigma = [sigma] * n_signals
    try:
        assert len(n_steps) == n_signals
        assert len(lam) == n_signals
        assert len(tau) == n_signals
        assert len(sigma) == n_signals
    except TypeError:
        raise ValueError(
            "Arguments must be int, float or a list of of length n_signals"
        )

    for i in range(n_signals):
        # Draw events at random from a Poisson distribution with mean
        # rate `lam`
        event_intervals = np.random.poisson(lam[i], n_events + 1)
        event_times = np.cumsum(event_intervals)
        events = np.zeros(n_steps[i])
        events[event_times[event_times < n_steps[i]]] = 1

        # Create an exponential decay kernel
        time = np.arange(n_steps[i])
        kernel = np.exp(-time / tau[i])

        # Ground-truth signal is the convolution of events with the kernel
        gt_signal = np.convolve(events, kernel, mode="full")[:n_steps[i]]

        # Add some zero-mean noise to define the observed noisy signal
        noise = np.random.normal(0, sigma[i], n_steps[i])
        noisy_signal = gt_signal + noise

        # Append signals to the list
        noisy_signals.append(noisy_signal)
        gt_signals.append(gt_signal)

    return noisy_signals, gt_signals
