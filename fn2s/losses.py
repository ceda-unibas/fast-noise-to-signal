import itertools

import numpy as np

from torch.nn.functional import mse_loss, binary_cross_entropy


def residual_plus_consistency(model, signal):

    # Normalize the signal to [0, 1]
    signal = signal - signal.min()
    signal = signal / signal.max()

    # Split the signal into a surrogate dataset
    splits = model.split_fun(signal)

    # Get residual prediction for the splits
    split_residuals = [split - model(split) for split in splits]

    # Loop over all pairs of splits and split residuals to compute the
    # residual part of the loss
    loss_res = 0.
    for i, j in itertools.combinations(range(len(splits)), 2):
        loss_res += mse_loss(splits[i], split_residuals[j])
        loss_res += mse_loss(splits[j], split_residuals[i])

    # Get residual prediction for the original signal
    signal_residual = signal - model(signal)

    # Split the residual into a surrogate dataset
    signal_residual_splits = model.split_fun(signal_residual)

    # Loop over the splits to compute the consistency part of the loss
    loss_cons = 0.
    for i, split in enumerate(splits):
        loss_cons += mse_loss(split, signal_residual_splits[i])

    # Compute the total loss
    loss = (0.5 * loss_res) + (0.5 * loss_cons)

    return loss


def bce_split_pairs(model, signal):

    # Split the signal into a surrogate dataset
    splits = model.split_fun(signal)

    # Get a random pair of splits
    i, j = np.random.choice(range(len(splits)), size=2, replace=False)

    # Compute the BCE loss for this pair of splits
    loss = binary_cross_entropy(splits[i], model(splits[j]))

    return loss
