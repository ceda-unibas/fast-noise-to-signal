import matplotlib.pyplot as plt
import numpy as np

from fn2s import utils


def plot_signal_and_splits(signal, split_fun, depth=1, figsize=(10, 6)):

    # Get splits
    splits = utils.recursive_split(signal, split_fun, depth=depth)

    # Convert signal and splits to numpy arrays
    signal = signal.detach().cpu().numpy().flatten()
    splits = [split.detach().cpu().numpy().flatten() for split in splits]

    # Define mosaic
    mosaic = [[],
              []]
    for i, split in enumerate(splits):
        mosaic[0].append("Signal")
        mosaic[1].append(f"Split {i + 1}")
    fig, axd = plt.subplot_mosaic(
        mosaic,
        layout='constrained',
        figsize=figsize,
    )

    # Set fixed y-axis limits
    y_diff = np.abs(np.max(signal) - np.min(signal))
    ylim = [np.min(signal) - 0.1 * y_diff,
            np.max(signal) + 0.1 * y_diff]

    # Original signal goes on top
    axd["Signal"].plot(signal)
    axd["Signal"].set(title="Signal", xticks=[], yticks=[], ylim=ylim,)

    # Split signals go on bottom
    for i, split in enumerate(splits):
        axd[f"Split {i + 1}"].plot(split)
        axd[f"Split {i + 1}"].set(title=f"Split {i + 1}",
                                  xticks=[], yticks=[], ylim=ylim,)

    return fig, axd
