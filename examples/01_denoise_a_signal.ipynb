{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Zero-shot denoise a signal"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%reload_ext autoreload\n",
    "%autoreload 2\n",
    "\n",
    "import nb_utils\n",
    "import torch\n",
    "\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "import plotly.express as px\n",
    "\n",
    "from fn2s import (\n",
    "    models,\n",
    "    utils,\n",
    ")\n",
    "from lightning import Trainer\n",
    "from lightning.pytorch.callbacks.early_stopping import EarlyStopping\n",
    "from torch.utils.data import DataLoader\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Load a sample noisy signal"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load numpy array\n",
    "noisy_signal = np.load('../data/noisy_signal.npy')\n",
    "\n",
    "# Convert to dataframe and plot with plotly\n",
    "noisy_signal_df = pd.DataFrame(noisy_signal, columns=['value'])\n",
    "fig = px.line(noisy_signal_df, y='value')\n",
    "\n",
    "# Zoom in to the last 100 points\n",
    "n_points = len(noisy_signal_df)\n",
    "fig.update_xaxes(range=[n_points - 1000, n_points])\n",
    "\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Splitting: create a training set from a single signal"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Split-and-squeeze"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "signal_tensor = torch.tensor(\n",
    "    noisy_signal_df[\"value\"].to_numpy()[-1000:-1],\n",
    "    dtype=torch.float32,\n",
    ").reshape(1, 1, -1)\n",
    "\n",
    "# Plot with splits at a certain depth\n",
    "fig, axd = nb_utils.plot_signal_and_splits(\n",
    "    signal_tensor,\n",
    "    lambda x : utils.split_and_squeeze(x),\n",
    "    depth=2,\n",
    "    figsize=(16, 6),\n",
    ")\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Split-and-average"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "signal_tensor = torch.tensor(\n",
    "    noisy_signal_df[\"value\"].to_numpy()[-1000:-1],\n",
    "    dtype=torch.float32,\n",
    ").reshape(1, 1, -1)\n",
    "\n",
    "# Plot with splits at a certain depth\n",
    "fig, axd = nb_utils.plot_signal_and_splits(\n",
    "    signal_tensor,\n",
    "    lambda x : utils.split_and_average(x, randomize=True, window_length=4),\n",
    "    depth=1,\n",
    "    figsize=(16, 6),\n",
    ")\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Training the denoiser with a \"residual regularized\" procedure\n",
    "\n",
    "Like in [(Mansour and Heckel 2023)](https://doi.org/10.1109/CVPR52729.2023.01347)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def split_fun(x):\n",
    "    return utils.recursive_split(\n",
    "        x,\n",
    "        lambda x : utils.split_and_average(x, randomize=True, window_length=4),\n",
    "        depth=1\n",
    "    )\n",
    "    # return utils.recursive_split(x, utils.split_and_squeeze, depth=3)\n",
    "\n",
    "model = models.FN2S(\n",
    "    training_type=\"residual_regularized\",\n",
    "    split_fun=split_fun,\n",
    "    net=models.FCNSmall(sigmoid_output=False),\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set up singleton dataset\n",
    "dataset = utils.SingletonDataset(noisy_signal)\n",
    "dataloader = DataLoader(dataset, batch_size=1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "trainer = Trainer(max_epochs=100, log_every_n_steps=1)\n",
    "trainer.fit(model, train_dataloaders=dataloader, val_dataloaders=dataloader)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Visualize the denoising"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get denoised signal from trained model\n",
    "model.eval()\n",
    "denoised_signal = model.denoise(signal_tensor)\n",
    "denoised_signal = denoised_signal.detach().numpy().flatten()\n",
    "\n",
    "# Plot\n",
    "fig, ax = plt.subplots(2,1, figsize=(10, 5))\n",
    "ax[0].plot(noisy_signal[-1000:-1])\n",
    "ax[0].set_title(\"Original signal\")\n",
    "ax[1].plot(denoised_signal[-1000:-1])\n",
    "ax[1].set_title(\"Denoised signal\")\n",
    "plt.tight_layout()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Training the denoiser with a \"BCE stopped\" procedure\n",
    "\n",
    "Like in [(Lequyer et al. 2022)](https://doi.org/10.1038/s42256-022-00547-8)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def split_fun(x):\n",
    "    # return utils.recursive_split(x, utils.split_and_average, depth=2)\n",
    "    return utils.recursive_split(x, utils.split_and_squeeze, depth=2)\n",
    "\n",
    "model = models.FN2S(\n",
    "    training_type=\"bce_stopped\",\n",
    "    split_fun=split_fun,\n",
    "    net=models.FCNSmall(sigmoid_output=True),\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set up singleton dataset\n",
    "dataset = utils.SingletonDataset(noisy_signal, yield_normalized=True)\n",
    "dataloader = DataLoader(dataset, batch_size=1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set up an EarlyStopping callback\n",
    "early_stopping = EarlyStopping(\n",
    "    monitor=\"val_loss\",\n",
    "    patience=10,\n",
    "    verbose=True,\n",
    "    mode=\"min\",\n",
    ")\n",
    "\n",
    "# Create a trainer\n",
    "trainer = Trainer(max_epochs=100,\n",
    "                  log_every_n_steps=1,\n",
    "                  callbacks=[early_stopping])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "trainer.fit(model, train_dataloaders=dataloader, val_dataloaders=dataloader)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Load model from checkpoint with best validation loss"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model = models.FN2S.load_from_checkpoint(\n",
    "    trainer.checkpoint_callback.best_model_path,\n",
    "    split_fun=split_fun,\n",
    "    net=models.FCNSmall(sigmoid_output=True),\n",
    "    training_type=\"bce_stopped\",\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Visualize the denoising"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get denoised signal from trained model\n",
    "model.eval()\n",
    "denoised_signal = model.denoise(signal_tensor)\n",
    "denoised_signal = denoised_signal.detach().numpy().flatten()\n",
    "\n",
    "# Plot\n",
    "fig, ax = plt.subplots(2,1, figsize=(10, 5))\n",
    "ax[0].plot(noisy_signal[-1000:-1])\n",
    "ax[0].set_title(\"Original signal\")\n",
    "ax[1].plot(denoised_signal[-1000:-1])\n",
    "ax[1].set_title(\"Denoised signal\")\n",
    "plt.tight_layout()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "denoised_signal"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "fn2s",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
