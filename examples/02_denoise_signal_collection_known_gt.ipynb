{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Denoise a collection of similar signals with known ground-truth\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%reload_ext autoreload\n",
    "%autoreload 2\n",
    "\n",
    "import os\n",
    "import torch\n",
    "\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "import plotly.express as px\n",
    "\n",
    "from fn2s import (\n",
    "    callbacks,\n",
    "    models,\n",
    "    signals,\n",
    "    utils,\n",
    ")\n",
    "from lightning import Trainer\n",
    "from lightning.pytorch import loggers as pl_loggers\n",
    "from lightning.pytorch.callbacks import ModelCheckpoint\n",
    "from torch.utils.data import DataLoader\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Create a collection of synthetic signals\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Generate a noisy train of exponential decay signals\n",
    "n_signals = 128\n",
    "n_steps = 1000\n",
    "lam = np.random.uniform(50, 500, n_signals)\n",
    "tau = np.random.uniform(10, 100, n_signals)\n",
    "sigma = np.random.uniform(0.05, 0.15, n_signals)\n",
    "noisy_signals, gt_signals = signals.make_noisy_exp_decay_train(\n",
    "    n_signals=n_signals,\n",
    "    n_steps=n_steps,\n",
    "    lam=lam,\n",
    "    tau=tau,\n",
    "    sigma=sigma,\n",
    ")\n",
    "\n",
    "# Plot the first 4 signals in parallel\n",
    "fig, ax = plt.subplots(4, 1, figsize=(10, 10), sharex=True, sharey=True)\n",
    "\n",
    "for i in range(4):\n",
    "    # Plot the ground truth and noisy signals with transparency\n",
    "    ax[i].plot(gt_signals[i], label='Ground truth', alpha=1)\n",
    "    ax[i].plot(noisy_signals[i], label='Noisy signal', alpha=0.5)\n",
    "    ax[i].legend()\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Denoising with a \"residual regularized\" procedure\n",
    "\n",
    "Like in [(Mansour and Heckel 2023)](https://doi.org/10.1109/CVPR52729.2023.01347)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "split_depth = 1\n",
    "def split_fun(x):\n",
    "    # return utils.recursive_split(\n",
    "    #     x,\n",
    "    #     lambda x : utils.split_and_average(x, randomize=True, window_length=4),\n",
    "    #     depth=split_depth,\n",
    "    # )\n",
    "    return utils.recursive_split(x, utils.split_and_squeeze, depth=split_depth)\n",
    "\n",
    "model = models.FN2S(\n",
    "    training_type=\"residual_regularized\",\n",
    "    split_fun=split_fun,\n",
    "    net=models.FCNSmall(sigmoid_output=False),\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set up data loader\n",
    "signal_list = [\n",
    "    signal.reshape(1, -1).astype(np.float32) for signal in noisy_signals\n",
    "]\n",
    "batch_size = 32\n",
    "num_workers = 4\n",
    "train_dataloader = DataLoader(\n",
    "    signal_list,\n",
    "    batch_size=batch_size,\n",
    "    num_workers=num_workers,\n",
    "    persistent_workers=True,\n",
    "    shuffle=True)\n",
    "val_dataloader = DataLoader(\n",
    "    signal_list,\n",
    "    batch_size=batch_size,\n",
    "    num_workers=num_workers,\n",
    "    persistent_workers=True,\n",
    "    shuffle=False)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define a logger\n",
    "tb_logger = pl_loggers.TensorBoardLogger(\n",
    "    save_dir=\"logs\",\n",
    "    name=\"residual_regularized_known_gt\",\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define a checkpoint saving callback\n",
    "checkpoint_callback = ModelCheckpoint(\n",
    "    filename=\"{epoch:02d}-{train_loss:.2f}-{val_loss:.2f}\",\n",
    "    save_top_k=3,\n",
    "    monitor=\"val_loss\",\n",
    "    mode=\"min\",\n",
    "    every_n_epochs=2,\n",
    "    save_on_train_epoch_end=True,\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define a callback to log the plot of the denoising of the first signal in the validation set\n",
    "idx = 0\n",
    "denoising_callback = callbacks.DenoisingPlotCallback(\n",
    "    torch.tensor(noisy_signals[idx]).float().unsqueeze(0),\n",
    "    every_n_epochs=10,\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a callback to log the total gradient norm\n",
    "gradient_norm_callback = callbacks.TotalGradNormCallback(\n",
    "    norm_type=2,\n",
    "    every_n_steps=10,\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a callback to the norms of the gradients of each layer\n",
    "layer_grad_callback = callbacks.LayerGradNormCallback(\n",
    "    norm_type=2,\n",
    "    every_n_steps=10,\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a trainer\n",
    "trainer = Trainer(\n",
    "    max_epochs=10,\n",
    "    logger=tb_logger,\n",
    "    log_every_n_steps=int(n_signals / batch_size),\n",
    "    callbacks=[\n",
    "        checkpoint_callback,\n",
    "        denoising_callback,\n",
    "        gradient_norm_callback,\n",
    "        layer_grad_callback,\n",
    "    ],\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Train the model\n",
    "trainer.fit(model,\n",
    "            train_dataloaders=train_dataloader,\n",
    "            val_dataloaders=val_dataloader)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Manually save the last checkpoint\n",
    "trainer.save_checkpoint(\n",
    "    os.path.join(trainer.logger.log_dir,\n",
    "                 \"checkpoints\",\n",
    "                 \"last.ckpt\")\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Visualize the denoising\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load model from checkpoint\n",
    "checkpoint_path = os.path.join(\n",
    "    trainer.logger.log_dir,\n",
    "    \"checkpoints\",\n",
    "    \"last.ckpt\"\n",
    "    # \"epoch=137-train_loss=0.14-val_loss=0.13.ckpt\"\n",
    ")\n",
    "model = models.FN2S.load_from_checkpoint(checkpoint_path)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Select one of the signals in the collection to denoise\n",
    "# idx = 0\n",
    "idx = np.random.randint(0, n_signals)\n",
    "signal_tensor = torch.tensor(noisy_signals[idx]).float().unsqueeze(0)\n",
    "\n",
    "# Get denoised signal from trained model\n",
    "model.eval()\n",
    "denoised_signal = model.denoise(signal_tensor)\n",
    "denoised_signal = denoised_signal.detach().numpy().flatten()\n",
    "\n",
    "# Get PSNR of denoised signal\n",
    "psnr_denoised = utils.psnr(gt_signals[idx], denoised_signal)\n",
    "\n",
    "# Plot\n",
    "fig, ax = plt.subplots(3,1, figsize=(10, 5), sharex=True, sharey=True)\n",
    "ax[0].plot(noisy_signals[idx])\n",
    "ax[0].set_title(\"Observed noisy signal (idx: {})\".format(idx))\n",
    "ax[1].plot(denoised_signal)\n",
    "ax[1].set_title(\"Denoised signal (PSNR: {:.2f})\".format(psnr_denoised))\n",
    "ax[2].plot(gt_signals[idx])\n",
    "ax[2].set_title(\"Ground truth signal\")\n",
    "\n",
    "plt.tight_layout()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
