# fast-noise-to-signal

[![license][license-badge]][license]

## Fast Noise2Signal

Zero-shot denoising of 1-dimensional signals. Fast.

## Description

Fast Noise2Signal builds upon [Noise2Fast][noise2fast] and [Zero-Shot Noise2Noise][zsn2n].

## Installation

1. Clone this repository and navigate to its root folder.
2. (Optional) Create a new environment, `fn2s`, by running `conda env create -f environment.yml`.
3. Install [PyTorch](https://pytorch.org/get-started/locally/), following the instructions for you system.
4. Install the package `fn2s` Python package: `pip install -e .`
5. (Optional) Install the dependencies for the `examples/` folder: `pip install -r examples/requirements.txt`

## Usage

TBD.

## Contributing

TBD.

## License

This project is released under the [BSD 3-Clause License][license].

## Project status

Ongoing.

[license]: LICENSE
[license-badge]: https://img.shields.io/badge/license-BSD-green
[noise2fast]: https://github.com/pelletierlab/Noise2Fast/tree/main
[zsn2n]: https://colab.research.google.com/drive/1i82nyizTdszyHkaHBuKPbWnTzao8HF9b